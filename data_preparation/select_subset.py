import os
import tarfile
from shutil import copyfile

def extract_all(folder_name):
	for file in os.listdir(os.getcwd()+"/"+folder_name):
		if file.endswith(".tar"):
			print(file)
			tar = tarfile.open(os.getcwd()+"/"+folder_name+"/"+file)
			tar.extractall(os.getcwd()+"/"+folder_name)
			tar.close()

def select_images(folder_name):
	os.mkdir(os.getcwd()+"/selected_images")
	
	file_number = 0
	for subject in os.listdir(os.getcwd()+"/"+folder_name):
		if not subject.endswith(".tar"):
			print("subject: {}".format(subject))
			for emotion in os.listdir(os.getcwd()+"/"+folder_name+"/"+subject):
				print("emotion: {}".format(emotion))
				if emotion not in os.listdir(os.getcwd()+"/selected_images"):
					os.mkdir(os.getcwd()+"/selected_images"+"/"+emotion)
						
				for take in os.listdir(os.getcwd()+"/"+folder_name+"/"+subject+"/"+emotion):
					images = [image for image in os.listdir(os.getcwd()+"/"+folder_name+"/"+subject+"/"+emotion+"/"+take)] #if image.endswith(".jpg")]
						
					if not len(images) == 0:
						selected_image = images[int(len(images)/2)]
						print(selected_image)
						source = os.getcwd()+"/"+folder_name+"/"+subject+"/"+emotion+"/"+take+"/"+selected_image
						destination = os.getcwd()+"/selected_images"+"/"+emotion+"/"+str(file_number)+selected_image
						copyfile(source, destination)
						file_number = file_number + 1
							
						
if __name__ == "__main__":
	target_folder = "subjects3"
	extract_all(target_folder)
	select_images(target_folder)