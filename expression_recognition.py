import cv2
import os
import random
import glob
from statistics import mean
import numpy as numpy

emotions = ["anger", "disgust",  "fear", "happiness", "neutral", "sadness", "surprise"]
fishface = cv2.face.FisherFaceRecognizer_create()
data = {}

# get all files into a list, shuffle list
def get_files():
	directory = "./processed_selected_images/"
	all_files = []
	for emotion in emotions:
		datapath = directory + emotion + "/*"
		files = glob.glob(datapath)
		labeled_files = []
		for file in files:
			labeled_files.append([file, emotion])
		all_files.extend(labeled_files)
	random.shuffle(all_files)
	return all_files

# create unique folds for k-fold cross validation
def make_folds(num_folds):
	k = num_folds
	folds = []

	images = get_files()
	fold_size = int(len(images)/k)
	for i in range(k-1):
		folds += [images[i*fold_size:(i+1)*fold_size]]
	folds += [images[(k-1)*fold_size:len(images)]]

	return folds

# return lists for training/testing images/labels & image preprocessing
def make_sets(data, num_folds, testing_fold):
	training_images = []
	training_labels = []
	testing_images = []
	testing_labels = []

	# fill lists for training; skip over testing fold
	for i in range(num_folds):
		if i == testing_fold:
			continue
		for data_tuple in data[i]:
			image = cv2.imread(data_tuple[0])
			gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
			# cropped = crop_face(gray)
			# if cropped:
			# 	training_images.append(cropped)
			# 	training_labels.append(emotions.index(data_tuple[1]))
			training_images.append(gray)
			training_labels.append(emotions.index(data_tuple[1]))

	# fill list for testing
	for data_tuple in data[testing_fold]:
		image = cv2.imread(data_tuple[0])
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		# cropped = crop_face(gray)
		# if cropped:
		# 	testing_images.append(cropped)
		# 	testing_labels.append(emotions.index(data_tuple[1]))
		testing_images.append(gray)
		testing_labels.append(emotions.index(data_tuple[1]))

	return training_images, training_labels, testing_images, testing_labels

# # Crop image to only show the face
# def crop_face(img):
# 	face1 = cv2.CascadeClassifier("haarcascade_frontalface_default.xml").detectMultiScale(img, scaleFactor=1.1, minNeighbors=10, minSize=(5, 5), flags=cv2.CASCADE_SCALE_IMAGE)
# 	face2 = cv2.CascadeClassifier("haarcascade_frontalface_alt2.xml").detectMultiScale(img, scaleFactor=1.1, minNeighbors=10, minSize=(5, 5), flags=cv2.CASCADE_SCALE_IMAGE)
# 	face3 = cv2.CascadeClassifier("haarcascade_frontalface_alt.xml").detectMultiScale(img, scaleFactor=1.1, minNeighbors=10, minSize=(5, 5), flags=cv2.CASCADE_SCALE_IMAGE)
# 	face4 = cv2.CascadeClassifier("haarcascade_frontalface_alt_tree.xml").detectMultiScale(img, scaleFactor=1.1, minNeighbors=10, minSize=(5, 5), flags=cv2.CASCADE_SCALE_IMAGE)

# 	if len(face1) == 1:
# 		faceFeatures = face
# 	elif len(face2) == 1:
# 		faceFeatures = face_two
# 	elif len(face3) == 1:
# 		faceFeatures = face_three
# 	elif len(face4) == 1:
# 		faceFeatures = face_four

# 	for (x, y, w, h) in faceFeatures:
# 		img = img[y:y+h, x:x+w]
# 		cropped = cv2.resize(img, (350, 350))
# 		return cropped
# 	return None

###### CODE TO TEST COMPUTER GENERATED FACES

def create_testset(directory, num_folds, testing_fold):
	all_files = []
	for emotion in emotions:
		datapath = directory + emotion + "/*"
		files = glob.glob(datapath)
		labeled_files = []
		for file in files:
			labeled_files.append([file, emotion])
		all_files.extend(labeled_files)
	random.shuffle(all_files)

	k = num_folds
	folds = []

	images = all_files
	fold_size = int(len(images)/k)
	for i in range(k-1):
		folds += [images[i*fold_size:(i+1)*fold_size]]
	folds += [images[(k-1)*fold_size:len(images)]]

	for data_tuple in folds[testing_fold]:
		image = cv2.imread(data_tuple[0])
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		cropped = crop_face(gray)
		if cropped:
			testing_images.append(cropped)
			testing_labels.append(emotions.index(data_tuple[1]))
	return testing_images, testing_labels

# train and test learning algorithm
def learn_emotions(nfolds):
	num_folds = nfolds
	folds = make_folds(num_folds)

	tot_correct = 0
	tot_incorrect = 0
	predictions_by_label = {}

	# Train/Test model for each fold
	for i in range(num_folds):

		# Assemble training/testing sets based on folds
		print("Training Fold {} of {}:".format(i+1, num_folds))
		training_images, training_labels, testing_images, testing_labels = make_sets(folds, num_folds, i)

		# Train Classifier
		print("\tTraining Classifier: {} images".format(len(training_images)))
		fishface.train(training_images, numpy.asarray(training_labels))

		# Test Classifier
		print("\tTesting Classifier: {} images".format(len(testing_images)))
		cnt = 0
		correct = 0
		incorrect = 0
		for image in testing_images:
			true_label = testing_labels[cnt]
			pred, conf = fishface.predict(image)

			# Update predictions_by_label dictionary
			if true_label not in predictions_by_label:
				predictions_by_label[true_label] = {}
			if pred not in predictions_by_label[true_label]:
				predictions_by_label[true_label][pred] = 0
			predictions_by_label[true_label][pred] += 1

			# Count for accuracy
			if pred == true_label:
				correct += 1
				tot_correct += 1
			else:
				incorrect += 1
				tot_incorrect += 1
			cnt += 1

		# Print Results for i-th fold
		fold_accuracy = float(correct/(correct + incorrect))
		print("\t\tAccuracy = {}".format(fold_accuracy))

	# Print Overall Results
	print("\nOverall Accuracy = {}".format(tot_correct/(tot_correct+tot_incorrect)))

	print("Predicted Emotions by True Label:")
	for label in predictions_by_label:
		print("\t{}:".format(emotions[int(label)]))
		for prediction in predictions_by_label[label]:
			print("\t\t{}: {}".format(emotions[int(prediction)], predictions_by_label[label][prediction]))

# Main Function
if __name__ == '__main__':
	learn_emotions(10)