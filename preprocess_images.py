import os
import cv2
import glob

src_directory = "./selected_images/"
dst_directory = "processed_selected_images"
emotions = ["anger", "disgust",  "fear", "happiness", "neutral", "sadness", "surprise"]

def crop_face(img):
	face1 = cv2.CascadeClassifier("haarcascade_frontalface_default.xml").detectMultiScale(img, scaleFactor=1.1, minNeighbors=10, minSize=(5, 5), flags=cv2.CASCADE_SCALE_IMAGE)
	face2 = cv2.CascadeClassifier("haarcascade_frontalface_alt2.xml").detectMultiScale(img, scaleFactor=1.1, minNeighbors=10, minSize=(5, 5), flags=cv2.CASCADE_SCALE_IMAGE)
	face3 = cv2.CascadeClassifier("haarcascade_frontalface_alt.xml").detectMultiScale(img, scaleFactor=1.1, minNeighbors=10, minSize=(5, 5), flags=cv2.CASCADE_SCALE_IMAGE)
	face4 = cv2.CascadeClassifier("haarcascade_frontalface_alt_tree.xml").detectMultiScale(img, scaleFactor=1.1, minNeighbors=10, minSize=(5, 5), flags=cv2.CASCADE_SCALE_IMAGE)

	if len(face1) == 1:
		faceFeatures = face1
	elif len(face2) == 1:
		faceFeatures = face2
	elif len(face3) == 1:
		faceFeatures = face3
	elif len(face4) == 1:
		faceFeatures = face4

	for (x, y, w, h) in faceFeatures:
		img = img[y:y+h, x:x+w]
		cropped = cv2.resize(img, (350, 350))
		return cropped
	return None

def preprocess():
    if not os.path.exists(dst_directory):
        os.makedirs('processed_selected_images')

    for emotion in emotions:
        if not os.path.exists(dst_directory  + '/' + emotion):
            os.makedirs(dst_directory + '/' + emotion)

        files = glob.glob(src_directory + emotion + "/*")
        for file in files:
            image = cv2.imread(file)
            gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            cropped = crop_face(gray_image)
            cv2.imwrite(dst_directory + '/' + emotion + '/' + file.split('/')[-1], cropped)

if __name__ == '__main__':
    preprocess()
