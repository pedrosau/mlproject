from PIL import Image
import numpy as np
import os
		
def load_data(emotion):
	bwdata = []

	for image in os.listdir(os.getcwd()+"/../googleset/"+emotion):
		image_path = os.getcwd()+"/../googleset/"+emotion+"/"+image
		bwdata.append(load_image(image_path))

	return ((np.array(bwdata), None),(None, None))

def load_image(filename):
    img = Image.open(filename)
    img.load()
    return np.asarray(img, dtype="int32")