import numpy as np
from image_helper import ImageHelper
from dcgan import DCGAN
import dataset

(X, _), (_, _) = dataset.load_data("anger")
# print(X.shape)
# print(X[0].shape)
# print(X[0])

X_train = X / 127.5 - 1.
X_train = np.expand_dims(X_train, axis=3)

# print(X_train.shape)
# print(X_train[0].shape)
# print(X_train[0])

image_helper = ImageHelper()
generative_advarsial_network = DCGAN(X_train[0].shape, 400, image_helper, 1)
generative_advarsial_network.train(100, X_train, batch_size=32)
